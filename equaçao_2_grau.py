import tkinter as tk
import math

class Eq_2_grau():
    def __init__(self,janela):
        self.janela = janela
        self.janela.title("Equaçoes Do 2 Grau")
        self.janela.geometry("300x200")

        self.frame1 = tk.Frame(self.janela)
        self.frame2 = tk.Frame(self.janela)
        self.frame3 = tk.Frame(self.janela)

        self.bt_calc = tk.Button(self.frame1,text="Calcular",command=self.calc)
        self.lb_result = tk.Label(self.frame1,text="Resultado: ")
        self.lb_delta = tk.Label(self.frame1,text="Delta: ")

        self.et_A = tk.Entry(self.frame2,width=5,text="valor de a")
        self.et_B = tk.Entry(self.frame2,width=5,text="valor de b")
        self.et_C = tk.Entry(self.frame2,width=5,text="valor de c")

        self.lb_A = tk.Label(self.frame3,text="a :")
        self.lb_B = tk.Label(self.frame3,text="b :")
        self.lb_C = tk.Label(self.frame3,text="c :")

        self.et_A.pack(side="left")
        self.et_B.pack(side="left")
        self.et_C.pack(side="left")

        self.lb_A.pack(side="left")
        self.lb_B.pack(side="left")
        self.lb_C.pack(side="left")

        self.bt_calc.pack()
        self.lb_delta.pack()
        self.lb_result.pack()

        self.frame2.pack(side="top")
        self.frame3.pack(side="top")
        self.frame1.pack(side="top")

    def calc(self):
        a = self.et_A.get()
        b = self.et_B.get()
        c = self.et_C.get()

        self.lb_A["text"] = "a :" + a
        self.lb_B["text"] = "b :" + b
        self.lb_C["text"] = "c: " + c
        
        a = int(a)
        b = int(b)
        c = int(c)

        temp = b**2 - 4 * a * c

        if temp > 0:
            delta = math.sqrt(temp)
            self.lb_delta["text"] = "Delta: " + str(delta)
            delta = float(delta)
            x1 = (-b + delta)/(2 * a)
            x2 = (-b - delta)/(2 * a)
            self.lb_result["text"] = "Resultado: x1: %.2f, x2: %.2f" %(x1,x2)
        else:
            self.lb_result["text"] = "Resultado: Não é real"
            self.lb_delta["text"] = "Delta: Negativo"

janela = tk.Tk()
eq_2 = Eq_2_grau(janela)
janela.mainloop()